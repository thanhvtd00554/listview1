package com.example.customadapterlistview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements IOnChildItemClick{

    ListView lvTraiCay;
    ArrayList<TraiCay> arrayTraiCay;
    TraiCayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        Anhxa();
        adapter = new TraiCayAdapter(this, R.layout.dong_trai_cay, arrayTraiCay);
        lvTraiCay.setAdapter(adapter);
    }

    private void Anhxa() {
        lvTraiCay = (ListView) findViewById(R.id.listviewTraiCay);
        arrayTraiCay = new ArrayList<>();

        arrayTraiCay.add(new TraiCay("Dâu Tây", "Dâu tây hơi ngon", R.drawable.dautay));
        arrayTraiCay.add(new TraiCay("Dưa Hấu", "Dưa hấu to đùng", R.drawable.duahau));
        arrayTraiCay.add(new TraiCay("Đu Đủ", "Đu Đủ ít hạt nè", R.drawable.dudu));
        arrayTraiCay.add(new TraiCay("Thanh Long", "Thanh long nhiều hạt lắm", R.drawable.thanhlong));
        arrayTraiCay.add(new TraiCay("Xoài", "Xoài không ngon", R.drawable.xoaingon));
        arrayTraiCay.add(new TraiCay("Dâu Tây", "Dâu tây hơi ngon", R.drawable.dautay));
        arrayTraiCay.add(new TraiCay("Đu Đủ", "Đu Đủ ít hạt nè", R.drawable.dudu));
        arrayTraiCay.add(new TraiCay("Thanh Long", "Thanh long nhiều hạt lắm", R.drawable.thanhlong));
    }

    @Override
    public void onItemChildClick(int position) {

    }
}
