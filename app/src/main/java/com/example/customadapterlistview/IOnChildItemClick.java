package com.example.customadapterlistview;

public interface IOnChildItemClick {
    void onItemChildClick(int position);
}
